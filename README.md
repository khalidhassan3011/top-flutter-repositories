## Top Flutter Repositories

This is a simple Flutter application that shows the `most starred` and `updated` GitHub repositories by searching with the keyword **Flutter**.

---

#### Environment

| OS      | SDK                            | Editor         | Testing Device            |
|:--      | :----:                         | :----:         |                      ---: |
|Mac      | Flutter 3.3.10 • channel stable| Android Studio | Emulator - Pixel 5 API 30 |
|         | Dart 2.18.6 • DevTools 2.15.0  |                | Simulator - Iphone 14     |
|         |                                |                | Real Device - Poco M2 pro |


#### State Management

- Getx


#### Architecture

- GetX pattern Architecture
- Repository pattern


#### Network

- **`GetConnect`**: Used GetConnect for network call. it provided by `Getx` package
- **`dartz`**: Used dartz for reduce code and centralize api call with error handle
- For more details please read [api_helper_impl.dart](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/tree/main/lib/app/repository/api_helper_impl.dart)


#### Packages

- **[sqflite](https://pub.dev/packages/sqflite)**: for local storage
    - <details>
        <summary>Reason of use sqflite</summary> 

      Our requirements was
        - **The repository list and repository details data that loaded once, should be saved for offline browsing**
        - **Each time by scrolling, fetch 10 new items.**

      <br>suppose we used `hive` as our local database. Then we need to store data using this(`{key : value}`) format.

      we have two sort options. Their is a possibility to same data for both option. In this(`{key : value}`) format we can't store both option data at once. so we need to different box. This is waste of storage. Also we need 10 items at once every time serialy. That is quite hard to manage.

      In other side `sqflite` is a `mySQL` database. it allowed us to easily query data and store it in a more efficient manner compared to using other options like Hive.

      In this project I create a database `repositoriesDb.db` and two tables `repositories`, `other`

      | id | mostStarSerial | updatedSerial | repoDetails | ownerDetails |
      |:-- | :----:         | :----:        |  :----:     | :---:        |
      |1   | 1              | -1            |  ---        | ----         |
      |2   | 2              | 2             |  ---        | ----         |
      |3   | -1             | 1             |  ---        | ----         |

      This is the `repository` table. Here -1 in `mostStarSerial` and `updatedSerial` means this repository are not available for that sort option

      | keyword        | value   |
      |:--             |  ---:   |
      |sortOption      | updated |
      |lastRefreshTime | ""      |

      This is the `other` table.

      For more information on how we implemented sqflite in this project, please refer to the
      [database_helper.dart](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/common/local_database/database_helper.dart),
      [local_database.dart](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/common/local_database/local_database.dart)
    </details>



- **[google_fonts](https://pub.dev/packages/google_fonts)**: for Fonts
- **[font_awesome_flutter](https://pub.dev/packages/font_awesome_flutter)**: for icons

[See other package](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/pubspec.yaml) used on project


#### Folder Structure

<details>
  <summary>View folder structure</summary>

  ``` bash
.
├── README.md
├── analysis_options.yaml
├── android
├── assets
│   └── images
│       ├── profile_image_bg.jpg
│       └── something_wrong.png
├── ios
├── lib
│   ├── app
│   │   ├── common
│   │   │   ├── app_info
│   │   │   │   └── app_info.dart
│   │   │   ├── extensions
│   │   │   │   └── extensions.dart
│   │   │   ├── local_database
│   │   │   │   ├── database_helper.dart
│   │   │   │   ├── local_database.dart
│   │   │   │   └── tables
│   │   │   │       └── local_database_tables.dart
│   │   │   ├── message
│   │   │   │   └── custom_message.dart
│   │   │   ├── style
│   │   │   │   └── theme.dart
│   │   │   ├── utils
│   │   │   │   ├── exports.dart
│   │   │   │   ├── initializer.dart
│   │   │   │   ├── logcat.dart
│   │   │   │   ├── type_def.dart
│   │   │   │   └── utils.dart
│   │   │   ├── values
│   │   │   │   ├── my_assets.dart
│   │   │   │   ├── my_color.dart
│   │   │   │   ├── my_constant_value.dart
│   │   │   │   └── my_strings.dart
│   │   │   └── widgets
│   │   │       └── custom_error_widget.dart
│   │   ├── enums
│   │   │   └── repository_sort_option.dart
│   │   ├── models
│   │   │   ├── owner.dart
│   │   │   └── repository.dart
│   │   ├── modules
│   │   │   ├── home
│   │   │   │   ├── bindings
│   │   │   │   │   └── home_binding.dart
│   │   │   │   ├── controllers
│   │   │   │   │   └── home_controller.dart
│   │   │   │   ├── interface
│   │   │   │   │   └── home_interface.dart
│   │   │   │   ├── model
│   │   │   │   │   └── search_repositories.dart
│   │   │   │   └── views
│   │   │   │       └── home_view.dart
│   │   │   └── repository_details
│   │   │       ├── bindings
│   │   │       │   └── repository_details_binding.dart
│   │   │       ├── controllers
│   │   │       │   └── repository_details_controller.dart
│   │   │       ├── interface
│   │   │       │   └── repository_details_interface.dart
│   │   │       └── views
│   │   │           └── repository_details_view.dart
│   │   ├── repository
│   │   │   ├── api_error_handel.dart
│   │   │   ├── api_helper.dart
│   │   │   ├── api_helper_impl.dart
│   │   │   └── api_urls.dart
│   │   ├── routes
│   │   │   ├── app_pages.dart
│   │   │   └── app_routes.dart
│   │   └── top_flutter_repositories.dart
│   └── main.dart
├── linux
├── macos
├── pubspec.yaml
├── screenshots
├── test
│   ├── unit_test.dart
│   └── widget_test.dart
├── web
└── windows

```
</details>


#### Screens / Pages

<details>
    <summary> Home Screen </summary>

<br>**Features**
- Sort Option in Appbar
- Load form server or local database when app start `Fig: 1`
- Api error handle `Fig: 2`
- Pull to refresh `Fig: 4`
- Error message when pull to refresh error `Fig: 5`
- Load next 10 items when scrolling `Fig: 6`
- Load more Internet error handle `Fig: 7`
- Dark theme `Fig: 8`
- Click repository item go to repository details page

|<img src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/1-initial_loading.jpeg" width="200" height="450" /> | <img src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/2-no_internet.jpeg" width="200" height="450" /> | <img src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/3-home.jpeg" width="200" height="450" /> | <img src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/4-pull_to_refresh.jpeg" width="200" height="450" /> |
|:--:| :--: | :--: | :--: |
| Fig: 1 | Fig: 2 | Fig: 3 | Fig: 4 |


|<img src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/5-invalid_refresh_hints.jpeg" width="200" height="450" /> | <img src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/6-load_more.jpeg" width="200" height="450" /> | <img src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/7-load_more_no_internet.jpeg" width="200" height="450" /> | <img src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/8-home_dark.jpeg" width="200" height="450" /> |
|:--:| :--: | :--: | :--: |
| Fig: 5 | Fig: 6 | Fig: 7 | Fig: 8 |

> For more information about [Home Screen](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/tree/main/lib/app/modules/home)


<br><br>Basic Flow of [home screen](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/home/controllers/home_controller.dart)


![home page diagram](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/home_screen_diagram.png)

</details>



<details>
    <summary> Repository Details Screen </summary>

<br>**Features**
- Repository Owner Information
    - Image, Name, Location, Bio,
    - Count public repository, stars, Folowing and followers
- Current Repository description shown as recently **visited repsitory** `Fig: 9`

|<img src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/9-repo_details.jpeg" width="200" height="450" />
|:--:|
| Fig: 9 |


<br><br>Basic Flow of [Repository details screen](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/repository_details/controllers/repository_details_controller.dart)


![home page diagram](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/details_screen_diagram.png)


</details>



#### Testing

- [Unit testing](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/test/unit_test.dart)
    - Extension test on Either<L, R>
    - Time Difference `difference in minute`, `time ago`


#### BS23 Flutter Task - 101 requirment VS my status

<details>
    <summary> see details </summary>


| Serial | Requirment      | Status                            | reference  |
|:--     | :----           | :----:                            | :----:     |
| 1 | Fetch repository list from GitHub API using "Flutter" as query keyword | Done| [api_helper_impl.dart#L59](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/repository/api_helper_impl.dart#L59) |
| 2 | The fetched data should be stored in a local database to permit the app to be used in offline mode | Done| [save in local](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/common/local_database/database_helper.dart#L19) <br>[fetch from local](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/common/local_database/database_helper.dart#L124) |
| 3 | Fetching the repository list should be paginated by scrolling. Each time by scrolling, fetch 10 new items | Done| [home_controller.dart#L48](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/home/controllers/home_controller.dart#L48) <br> `Fig: 6` |
| 4 | The required data can be refreshed from the API no more frequently than once every 30 minutes. | Done| [home_controller.dart#L59](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/home/controllers/home_controller.dart#L171)  <br> `Fig: 5` |
| 5 | Show the list of repositories on the home page | Done| [home_view.dart#L167](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/home/views/home_view.dart#L167) <br> `Fig: 3` |
| 6 | List can be sorted by either the last updated date-time or star count (add a sorting button/icon) | Done| [home_view.dart#L19](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/home/views/home_view.dart#L19) <br> `Fig: 1` appbar |
| 7 | Selected sorting option persists in further app sessions. | Done|[save in local](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/home/controllers/home_controller.dart#L208) <br> [fetch from local](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/home/controllers/home_controller.dart#L89) |
| 8 | A repo details page, which is navigated by clicking on an item from the list | Done| [home_controller.dart#L201](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/home/controllers/home_controller.dart#L201) |
| 9 | Details page shows repo owner's name, photo, repository's description, last update date time in month-day-year hour:seconds format, each field in 2 digit numbers and any other fields you want | Done| [repository_details_view.dart](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/repository_details/views/repository_details_view.dart) |
| 10 | The repository list and repository details data that loaded once, should be saved for offline browsing. | Done| follow serial 2 |
| extra credits 1 | Unit Testing coverage | Done| [unit_test.dart](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/test/unit_test.dart) |
| extra credits 2 | UI Testing coverage | incomplete | |
| extra credits 3 | Unit Testing coverage | Done| [pubspec.yaml#L12](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/pubspec.yaml#L12) |
| extra credits 4 | Dependency Injection framework | Done| followed by extra credits 3 |
| extra credits 5 | Repository pattern with an appropriate abstraction layer. | Done| [api_helper.dart](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/repository/api_helper.dart) <br>[home_interface.dart](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/modules/home/interface/home_interface.dart) |
| extra credits 6 | Central API call error handling | Done| [api_helper_impl.dart#L38](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/blob/main/lib/app/repository/api_helper_impl.dart#L38) |
| extra credits 7 | App’s flavor (ex: DEV, QA, PROD, etc). | incomplete| have a basic idea but not implement ever any project |
| extra credits 8 | Right now there is no need for adding auth tokens with API calls. But you can implement the way to handle it for the future. Also can add some retry mechanism when the API fails. | incomplete | |

</details>


#### App journey Screen record

| gif  | video  | download URL |
|:--:  | :----: | :----:   |
| <img src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/screen_record_gif.gif" width="200" height="450" /> |  <video width="210" height="450" controls> <source src="https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/screen_record.mp4" type="video/mp4"> Your browser does not support the video tag. </video> | [`APK`](https://gitlab.com/khalidhassan3011/top-flutter-repositories/-/raw/main/screenshots/demo_apk_khalid.apk) |



#### Future development plans

- add a `tabview` in repository details page. Tabs are repositories, languages, commits
- a separate page for **follower** and **following** user list


>> Thank you to the BS23 team for giving me this opportunity. I am also grateful to those who have taken the time to read this README. It would be an honor to receive your valuable feedback. Thank you again for your support and I look forward to your contributions.





