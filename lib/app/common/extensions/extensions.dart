import 'package:dartz/dartz.dart';

extension EitherX<L, R> on Either<L, R> {
  // success
  R get asRight => (this as Right).value;

  // fail
  L get asLeft => (this as Left).value;
}

extension TimeDifferenceInMinute on String {
  int get timeDifferenceInMinute =>
      DateTime.now().difference(DateTime.parse(this)).inMinutes;

  String get timeAgo {
    int prefix = timeDifferenceInMinute;
    String postfix = "m";

    if ((timeDifferenceInMinute ~/ 60) ~/ 24 > 364) {
      prefix = ((timeDifferenceInMinute ~/ 60) ~/ 24) ~/ 365;
      postfix = "y";
    }
    else if (timeDifferenceInMinute ~/ 60 > 23) {
      prefix = (timeDifferenceInMinute ~/ 60) ~/ 24;
      postfix = "d";
    } else if (timeDifferenceInMinute > 59) {
      prefix = timeDifferenceInMinute ~/ 60;
      postfix = "h";
    }

    return "$prefix$postfix";
  }
}

extension CompactShort on num {
  String get compactShort {
    if (this < 1000) return toString();

    return "${(this / 1000).toStringAsFixed(1)}k";
  }
}
