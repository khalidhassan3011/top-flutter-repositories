// ignore: depend_on_referenced_packages
import 'package:path/path.dart';
import 'dart:convert';

import 'package:sqflite/sqflite.dart';

import '../../enums/repository_sort_option.dart';
import '../../models/owner.dart';
import '../../models/repository.dart';
import '../utils/logcat.dart';
import 'tables/local_database_tables.dart';

part 'local_database.dart';

class StorageHelper {
  StorageHelper._();

  /// save
  static Future saveRepositories(
    List<Repository> repositories,
    RepositorySortOption sortOption,
  ) async {

    List columnNameAndSerial = await _getRepositoryColumnNameAndLastSerial(sortOption);
    int lastSerialExist = columnNameAndSerial.last;

    for (int i = 0; i < repositories.length; i++) {
      Repository repository = repositories[i];

      String query = '''
        SELECT * 
        FROM ${_StorageKeyword.repositoriesTable}
        WHERE ${RepositoryTable.id} == ${repository.id}
      ''';
      var result = await _LocalDatabase.instance.read(query);

      // if not data exist with this id then insert
      if (result.length == 0) {
        lastSerialExist++;

        String insertQuery = '''
        INSERT INTO ${_StorageKeyword.repositoriesTable} (
          ${RepositoryTable.id},
          ${RepositoryTable.mostStarSerial},
          ${RepositoryTable.updatedSerial},
          ${RepositoryTable.repoDetails},
          ${RepositoryTable.ownerDetails}
        )
        VALUES (?, ?, ?, ?, ?)
        ''';

        int result = await _LocalDatabase.instance.create(insertQuery, [
          repository.id,
          columnNameAndSerial.first == RepositoryTable.mostStarSerial ? lastSerialExist : -1,
          columnNameAndSerial.first == RepositoryTable.updatedSerial ? lastSerialExist : -1,
          jsonEncode(repository.toJson()),
          jsonEncode(repository.owner?.toJson())
        ]);

        if (result == -1) {
          Logcat.msg("Failed to insert. id: ${repository.id}");
        }

      }
      // update
      else {
        await updateRepositorySortOptionSerial(repository, sortOption);
      }
    }
  }

  static Future saveSortOption(RepositorySortOption sortOption) async {
    String insertQuery = '''
        INSERT INTO ${_StorageKeyword.otherTable} (
          ${OtherTable.keyword},
          ${OtherTable.value}
        )
        VALUES (?, ?)
        ''';

    int result = await _LocalDatabase.instance.create(insertQuery, [
      _StorageKeyword.sortOption,
      sortOption.name
    ]);

    return result;
  }

  static Future saveLastRefreshTime(DateTime time) async {
    String insertQuery = '''
        INSERT INTO ${_StorageKeyword.otherTable} (
          ${OtherTable.keyword},
          ${OtherTable.value}
        )
        VALUES (?, ?)
        ''';

    int result = await _LocalDatabase.instance.create(insertQuery, [
      _StorageKeyword.lastRefreshTime,
      time.toString()
    ]);

    return result;
  }


  /// read
  static Future<List<Repository>> getAllMostStarRepositories() async {
    return await _getAllRepositories(RepositorySortOption.mostStar).then((value) => value);
  }

  static Future<List<Repository>> getAllUpdatedRepositories() async {
    return await _getAllRepositories(RepositorySortOption.updated).then((value) => value);
  }

  static Future<int> totalMostStarRepositories() async {
    return await _countUniqueRow(RepositoryTable.mostStarSerial);
  }

  static Future<int> totalUpdatedRepositories() async {
    return await _countUniqueRow(RepositoryTable.updatedSerial);
  }

  static Future<List<Repository>> getRepositories(
    RepositorySortOption sortOption,
    int page, {
    int limit = 10,
  }) async {

    List columnNameAndSerial = await _getRepositoryColumnNameAndLastSerial(sortOption);
    String column = columnNameAndSerial.first;

    String query = '''
      SELECT * 
      FROM ${_StorageKeyword.repositoriesTable} 
      WHERE $column != -1 AND $column > ${page * 10 - 10}
      ORDER BY $column ASC
      LIMIT $limit
    ''';

    var result = await _LocalDatabase.instance.read(query);

    return _resultToObject(result);
  }

  static Future<RepositorySortOption> getSortOptions() async {
    String query = '''
      SELECT * 
      FROM ${_StorageKeyword.otherTable} 
      WHERE ${OtherTable.keyword} = ?
    ''';

    var result = await _LocalDatabase.instance.read(query, [
      _StorageKeyword.sortOption
    ]);

    if(result.isEmpty) return RepositorySortOption.mostStar;

    return RepositorySortOption.values
        .where((element) => element.name == result.first[OtherTable.value])
        .first;
  }

  static Future<DateTime> getLastRefreshTime() async {
    String query = '''
      SELECT * 
      FROM ${_StorageKeyword.otherTable} 
      WHERE ${OtherTable.keyword} = ?
    ''';

    var result = await _LocalDatabase.instance.read(query, [
      _StorageKeyword.lastRefreshTime
    ]);

    if (result.isEmpty) return DateTime.now().subtract(const Duration(hours: 1));

    return DateTime.parse(result.first[OtherTable.value]);
  }


  /// update
  static Future updateRepositorySortOptionSerial(Repository repository, RepositorySortOption sortOption) async {

    List columnNameAndSerial = await _getRepositoryColumnNameAndLastSerial(sortOption);

    String updateQuery = '''
      UPDATE ${_StorageKeyword.repositoriesTable} 
      SET ${columnNameAndSerial.first} = ?
      WHERE ${RepositoryTable.id} = ?
    ''';

    return await _LocalDatabase.instance.update(updateQuery, [
      columnNameAndSerial.last,
      repository.id,
    ]);
  }

  static Future updateOption(RepositorySortOption sortOption) async {

    String updateQuery = '''
      UPDATE ${_StorageKeyword.otherTable}
      SET ${OtherTable.value} = ?
      WHERE ${OtherTable.keyword} = ?
    ''';

    int result = await _LocalDatabase.instance.update(updateQuery, [
      sortOption.name,
      _StorageKeyword.sortOption,
    ]);

    if(result == 0) {
      return saveSortOption(sortOption);
    }

    return result;
  }

  static Future updateLastSaveTime(DateTime time) async {

    String updateQuery = '''
      UPDATE ${_StorageKeyword.otherTable}
      SET ${OtherTable.value} = ?
      WHERE ${OtherTable.keyword} = ?
    ''';

    int result = await _LocalDatabase.instance.update(updateQuery, [
      time.toString(),
      _StorageKeyword.lastRefreshTime,
    ]);

    if(result == 0) {
      return saveLastRefreshTime(time);
    }

    return result;
  }


  /// helper
  // List return [column name(string), last serial(int)];
  static Future<List> _getRepositoryColumnNameAndLastSerial(RepositorySortOption sortOption) async {

    String columnName = "";
    int lastSerialExist = 0;

    switch (sortOption) {
      case RepositorySortOption.mostStar:
        columnName = RepositoryTable.mostStarSerial;
        lastSerialExist = await totalMostStarRepositories();
        break;
      case RepositorySortOption.updated:
        columnName = RepositoryTable.updatedSerial;
        lastSerialExist = await totalUpdatedRepositories();
        break;
    }

    return [columnName, lastSerialExist];
  }

  static Future<int> _countUniqueRow(String column) async {
    String query = '''
      SELECT COUNT(*) 
      FROM ${_StorageKeyword.repositoriesTable} 
      WHERE $column != -1
    ''';

    var count = await _LocalDatabase.instance.read(query);
    return Sqflite.firstIntValue(count) ?? 0;
  }

  static Future<List<Repository>> _getAllRepositories(RepositorySortOption sortOption) async {

    List columnNameAndSerial = await _getRepositoryColumnNameAndLastSerial(sortOption);
    String column = columnNameAndSerial.first;

    String query = '''
      SELECT * 
      FROM ${_StorageKeyword.repositoriesTable}
      WHERE $column != -1
      ORDER BY $column ASC
    ''';

    var result = await _LocalDatabase.instance.read(query);

    return _resultToObject(result);
  }

  static List<Repository> _resultToObject(result) {
    List<Repository> repositories = [];

    for (var item in result) {
      Repository repository = Repository.fromJson(jsonDecode(item[RepositoryTable.repoDetails]));
      repository.owner = Owner.fromJson(jsonDecode(item[RepositoryTable.ownerDetails]));

      repositories.add(repository);
    }

    return repositories;
  }
}
