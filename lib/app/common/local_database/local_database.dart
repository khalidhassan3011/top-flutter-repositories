part of 'database_helper.dart';

class _StorageKeyword {
  // database
  static const String repositoriesDb = 'repositoriesDb.db';

  // tables
  static const String repositoriesTable = 'repositories';
  static const String otherTable = 'other';

  // fields
  static const String sortOption = "sortOption";
  static const String lastRefreshTime = "lastRefreshTime";
}

class _LocalDatabase {
  static final _LocalDatabase instance = _LocalDatabase._init();
  static Database? _database;

  _LocalDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDB(_StorageKeyword.repositoriesDb);
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);
    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    await db.execute('''
      CREATE TABLE ${_StorageKeyword.repositoriesTable} ( 
      ${RepositoryTable.id} 'INTEGER PRIMARY KEY',
      ${RepositoryTable.mostStarSerial} 'INTEGER NOT NULL',
      ${RepositoryTable.updatedSerial} 'INTEGER NOT NULL',
      ${RepositoryTable.repoDetails} 'TEXT NOT NULL',
      ${RepositoryTable.ownerDetails} 'TEXT NOT NULL'
      )
    ''');

    await db.execute('''
      CREATE TABLE ${_StorageKeyword.otherTable} ( 
      ${OtherTable.keyword} 'TEXT NOT NULL',
      ${OtherTable.value} 'TEXT NOT NULL'
      )
    ''');
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }

  /// create
  Future<int> create(String query, [List<Object?>? arguments]) async {
    final db = await instance.database;
    return await db.rawInsert(query, arguments);
  }

  Future read(String query, [List<Object?>? arguments]) async {
    final db = await instance.database;
    return await db.rawQuery(query, arguments);
  }

  Future update(String query, [List<Object?>? arguments]) async {
    final db = await instance.database;
    return await db.rawUpdate(query, arguments);
  }

  Future delete(String query) async {
    final db = await instance.database;
    return await db.rawDelete(query);
  }
}
