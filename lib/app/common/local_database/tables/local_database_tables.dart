class RepositoryTable {
  static const String id = 'id';
  static const String mostStarSerial = 'mostStarSerial';
  static const String updatedSerial = 'updatedSerial';
  static const String repoDetails = 'repoDetails';
  static const String ownerDetails = 'ownerDetails';
}

class OtherTable {
  static const String keyword = 'keyword';
  static const String value = 'value';
}
