import '../utils/exports.dart';

class CustomMessage {
  CustomMessage._();

  static void snackbar(String msg) {
    ScaffoldMessenger.of(Get.context!).showSnackBar(
      SnackBar(content: Text(msg)),
    );
  }
}
