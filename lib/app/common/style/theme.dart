import 'package:google_fonts/google_fonts.dart';

import '../utils/exports.dart';

class AppTheme {
  AppTheme._();

  static ThemeData get light => ThemeData.light().copyWith(
        scaffoldBackgroundColor: MyColors.f6f6f6,
        appBarTheme: const AppBarTheme(
          backgroundColor: MyColors.f6f6f6,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        iconTheme: const IconThemeData(color: Colors.black),
        textTheme: TextTheme(
          headline1: const TextStyle(color: Colors.black),
          bodyText1: GoogleFonts.mulish(color: Colors.black, fontWeight: FontWeight.normal),
        ),
        cardColor: Colors.white,
      );

  static ThemeData get dark => ThemeData.dark().copyWith(
        scaffoldBackgroundColor: MyColors.black26,
        appBarTheme: const AppBarTheme(
          backgroundColor: MyColors.black26,
          iconTheme: IconThemeData(color: Colors.white),
        ),
        iconTheme: const IconThemeData(color: Colors.white),
        textTheme: TextTheme(
          headline1: const TextStyle(color: Colors.white),
          bodyText1: GoogleFonts.mulish(color: Colors.white, fontWeight: FontWeight.normal),
        ),
        cardColor: Colors.black,
      );
}
