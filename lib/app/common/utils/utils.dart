class Utils {
  Utils._();

  static String dateFormat(String dateInString) {
    DateTime date = DateTime.parse(dateInString);

    return "${date.month.toString().padLeft(2,'0')}-${date.day.toString().padLeft(2,'0')}-${date.year.toString().substring(2)} ${date.hour.toString().padLeft(2,'0')}-${date.minute.toString().padLeft(2,'0')}";
  }
}
