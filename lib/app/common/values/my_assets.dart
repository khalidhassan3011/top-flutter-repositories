class MyAssets {
  MyAssets._();

  static const somethingWrong = "assets/images/something_wrong.png";
  static const profileImageBg = "assets/images/profile_image_bg.jpg";
}
