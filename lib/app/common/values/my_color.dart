import 'package:flutter/material.dart';

class MyColors {
  MyColors._();

  static const submitError = Color(0xFF70DAAD);

  // color with
  static const f6f6f6 = Color(0xFFF6F6F6);
  static const black26 = Color.fromARGB(255, 34, 34, 34);
}