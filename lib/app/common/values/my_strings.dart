class MyStrings {
  MyStrings._();

  static const String reportError = "Report Error";
  static const String errorReportSuccessfully = "Report Error Successfully";

  /// api error message
  static const String noInternetConnection = "No Internet Connection";
  static const String requestTimeout = "Request Timeout";
  static const String invalidFormat = "Invalid Format";
  static const String somethingWrong = "Something Wrong";

  /// arguments
  static const String data = "data";

  /// ui const
  static const String follow = "Follow";
  static const String refreshError = "You can't refresh before 30 minutes from your last refresh";
}
