enum RepositorySortOption {
  mostStar("Most Start", "stars"),
  updated("Updated", "updated");

  /// [title] used on view
  /// ex: dropdown
  final String title;

  /// [urlKeyword] used when API call
  final String urlKeyword;

  const RepositorySortOption(
    this.title,
    this.urlKeyword,
  );
}
