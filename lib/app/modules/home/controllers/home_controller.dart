import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../common/local_database/database_helper.dart';
import '../../../common/utils/exports.dart';
import '../../../common/utils/logcat.dart';
import '../../../common/values/my_constant_value.dart';
import '../../../enums/repository_sort_option.dart';
import '../../../models/repository.dart';
import '../../../repository/api_helper.dart';
import '../../../routes/app_pages.dart';
import '../interface/home_interface.dart';

class HomeController extends GetxController implements HomeInterface {

  final ApiHelper _apiHelper = Get.find();

  /// [repositories] store all repositories. that can be from local or server
  RxList<Repository> repositories = <Repository>[].obs;

  /// [errorMsg] contains msg generated form api response or invalid format
  RxString errorMsg = "".obs;

  /// [initialLoading] will true until [_setInitialValue] not execute
  RxBool initialLoading = true.obs;

  /// when [scrollController] reached to end [loadMore] will true and
  /// fetch next [MyConstantValue.numOfRepositoryPerLoad] data
  ///
  /// [loadMore] is observable because we need to change ui based on
  /// [loadMore] value
  RxBool loadMore = false.obs;

  /// [_loadMoreFromServer] used for prevent multiple api call before finished
  /// previous call
  RxBool loadMoreFromServer = false.obs;

  /// when [_isRefreshed] will ture then fetch data form api and update previous
  bool _isRefreshed = false;

  /// [selectedSortOption] set observable because
  /// when [selectedSortOption] change we need to change ui and set new
  /// repositories in [repositories]
  Rx<RepositorySortOption> selectedSortOption = RepositorySortOption.mostStar.obs;

  int _page = 1;

  /// used [scrollController] for pagination
  late ScrollController scrollController;

  /// if anyone try pull to refresh before 30 minutes
  /// then [pullToRefreshError] has data
  /// if [pullToRefreshError] has data an hints show on top of home page
  RxString pullToRefreshError = "".obs;

  /// ui
  IconData themeChangeIcon = FontAwesomeIcons.lightbulb;


  /// getter
  bool get isSortOptionMostStart => selectedSortOption.value == RepositorySortOption.mostStar;
  bool get isSortOptionUpdate => selectedSortOption.value == RepositorySortOption.updated;

  @override
  void onInit() {
    scrollController = ScrollController()..addListener(_scrollListener);
    _setInitialValue();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void _scrollListener() {
    if (scrollController.position.extentAfter == 0) {
      _getRepositories();
    }
  }

  /// if we have storeOption in our local database then
  /// set it [selectedSortOption] and get repositories
  Future<void> _setInitialValue() async {
    selectedSortOption.value = await StorageHelper.getSortOptions();

    _getRepositories();
  }

  Future<void> _getRepositories() async {
    int totalRepo = await StorageHelper.totalMostStarRepositories();

    if(isSortOptionUpdate) totalRepo = await StorageHelper.totalUpdatedRepositories();

    if(_isRefreshed || totalRepo == 0 || totalRepo == repositories.length) {
      _fetchRepositoriesFromServer();
    } else {
      _fetchRepositoryFromLocalDatabase();
    }
  }

  Future<void> _fetchRepositoryFromLocalDatabase() async {
    if (loadMore.value) return;
    if (!initialLoading.value) loadMore.value = true;

    Logcat.msg("Fetch form OFFLINE");

    List<Repository> data = await StorageHelper.getRepositories(
      selectedSortOption.value,
      _page,
      limit: MyConstantValue.numOfRepositoryPerLoad,
    );

    repositories..addAll(data)..refresh();

    _page++;

    loadMore.value = false;
    initialLoading.value = false;
  }

  Future<void> _fetchRepositoriesFromServer() async {
    if(loadMoreFromServer.value) return;
    Logcat.msg("Fetch form ONLINE");

    loadMoreFromServer.value = true;

    await _apiHelper.getSearchRepositories(
      page: _page,
      perPageItem: MyConstantValue.numOfRepositoryPerLoad,
      sortBy: selectedSortOption.value
    ).then((response) {
      response.fold((l) {
        loadMoreFromServer.value = false;
        initialLoading.value = false;
        errorMsg.value = l;
      }, (r) {
        errorMsg.value = "";
        _saveRepositoryInLocal(r.items ?? []);
      });
    });
  }

  Future<void> _saveRepositoryInLocal(List<Repository> list) async {
    await StorageHelper.saveRepositories(list, selectedSortOption.value);

    initialLoading.value = false;
    loadMore.value = false;
    loadMoreFromServer.value = false;
    _page++;

    repositories..addAll(list)..refresh();
  }

  void _reset() {
    initialLoading.value = true;
    loadMore.value = false;
    loadMoreFromServer.value = false;
    _page = 1;

    repositories..clear()..refresh();

    _getRepositories();
  }

  @override
  Future <void> onRefresh() async {
    if(repositories.isEmpty) {
      errorMsg.value = "";
      _reset();
      return;
    }

    DateTime lastRefreshTime = await StorageHelper.getLastRefreshTime();
    if (_isRefreshed || lastRefreshTime.toString().timeDifferenceInMinute < 30) {
      pullToRefreshError.value = "Wait ${30 - lastRefreshTime.toString().timeDifferenceInMinute} minutes for refresh";
      _setTimerForDismissRefreshError();
    } else {
      pullToRefreshError.value = "";
      _isRefreshed = true;
      _reset();
      _updateLastRefreshTime();
    }
  }

  void _setTimerForDismissRefreshError() {
    Future.delayed(const Duration(seconds: 5), () {
      pullToRefreshError.value = "";
    });
  }

  Future<void> _updateLastRefreshTime() async {
    await StorageHelper.saveLastRefreshTime(DateTime.now());
  }

  @override
  void onRepositoryClick(int index) {
    Get.toNamed(Routes.repositoryDetails, arguments: {
      MyStrings.data : repositories[index]
    });
  }

  @override
  void onSortOptionChange(RepositorySortOption sortOption) {
    if(selectedSortOption.value != sortOption) {
      selectedSortOption.value = sortOption;
      selectedSortOption.refresh();

      StorageHelper.updateOption(selectedSortOption.value);

      _reset();
    }
  }

  @override
  void onThemeChange() {
    if (Get.isDarkMode) {
      Get.changeThemeMode(ThemeMode.light);
      themeChangeIcon = FontAwesomeIcons.moon;
    } else {
      Get.changeThemeMode(ThemeMode.dark);
      themeChangeIcon = FontAwesomeIcons.lightbulb;
    }
  }
}
