import '../../../enums/repository_sort_option.dart';

abstract class HomeInterface {
  void onRefresh();

  void onSortOptionChange(RepositorySortOption sortOption);

  void onRepositoryClick(int index);

  void onThemeChange();
}
