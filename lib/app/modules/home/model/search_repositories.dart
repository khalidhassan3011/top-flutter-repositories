import 'dart:convert';

import '../../../models/repository.dart';

class SearchRepositories {
  SearchRepositories({
    this.totalCount,
    this.incompleteResults,
    this.items,
  });

  final int? totalCount;
  final bool? incompleteResults;
  final List<Repository>? items;

  factory SearchRepositories.fromRawJson(String str) => SearchRepositories.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory SearchRepositories.fromJson(Map<String, dynamic> json) => SearchRepositories(
    totalCount: json["total_count"],
    incompleteResults: json["incomplete_results"],
    items: json["items"] == null ? [] : List<Repository>.from(json["items"]!.map((x) => Repository.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "total_count": totalCount,
    "incomplete_results": incompleteResults,
    "items": items == null ? [] : items == null ? [] : List<dynamic>.from(items!.map((x) => x.toJson())),
  };
}

