import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../../common/utils/exports.dart';
import '../../../enums/repository_sort_option.dart';
import '../../../models/repository.dart';
import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: _appbarTitle(context),
        actions: [
          _sortOptions(context),
          const SizedBox(width: 8),
          GestureDetector(
            onTap: controller.onThemeChange,
            child: Container(
              padding: const EdgeInsets.all(6),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(color: Colors.blueAccent),
              ),
              child: Icon(
                controller.themeChangeIcon,
                size: 14,
              ),
            ),
          ),
          const SizedBox(width: 8),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: controller.onRefresh,
        child: Obx(
          () => controller.initialLoading.value
              ? const Center(child: CircularProgressIndicator())
              : controller.errorMsg.value.isNotEmpty && controller.repositories.isEmpty
                  ? Stack(
                      children: [
                        Center(
                          child: Text(
                            controller.errorMsg.value,
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                        ),
                        ListView(),
                      ],
                    )
                  : Column(
                      children: [
                        _onRefreshError,
                        _showAllRepositories,
                      ],
                    ),
        ),
      ),
    );
  }

  Widget _appbarTitle(BuildContext context) => Row(
        children: [
          const FaIcon(
            FontAwesomeIcons.github,
          ),
          const SizedBox(width: 12),
          Text(
            "Git",
            style: GoogleFonts.lato(
              color: Colors.grey,
              fontWeight: FontWeight.normal,
            ),
          ),
          const SizedBox(width: 7),
          Text(
            "Repositories",
            style: GoogleFonts.lato(
              textStyle: Theme.of(context).textTheme.headline1,
              fontSize: 20,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      );

  Widget _sortOptions(BuildContext context) => Container(
        padding: const EdgeInsets.fromLTRB(15, 0, 5, 0),
        margin: const EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          borderRadius: BorderRadius.circular(50),
          border: Border.all(color: Colors.amber),
        ),
        child: Obx(
          () => DropdownButtonHideUnderline(
            child: DropdownButton<RepositorySortOption>(
              value: controller.selectedSortOption.value,
              items: <DropdownMenuItem<RepositorySortOption>>[
                ...RepositorySortOption.values.map((e) {
                  return DropdownMenuItem(
                    value: e,
                    child: Text(
                      e.title.capitalize!,
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  );
                })
              ],
              onChanged: (value) => controller.onSortOptionChange(value!),
            ),
          ),
        ),
      );

  Widget get _onRefreshError => Obx(
        () => Visibility(
          visible: controller.pullToRefreshError.value.isNotEmpty,
          child: Container(
            margin: const EdgeInsets.all(16),
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.blue.shade50,
              borderRadius: BorderRadius.circular(6),
              border: Border.all(color: Colors.blue.shade200),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Icon(
                  Icons.info_outlined,
                  color: Colors.blue,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        MyStrings.refreshError,
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.blue,
                        ),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        controller.pullToRefreshError.value,
                        style: const TextStyle(
                          color: Colors.blue,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );

  Widget get _showAllRepositories => Expanded(
        child: ListView.builder(
          controller: controller.scrollController,
          itemCount: controller.repositories.length,
          itemBuilder: (context, index) {

            if(index == controller.repositories.length - 1) {
              return Column(
                children: [
                  _repoItem(context, controller.repositories[index], index),

                  const SizedBox(height: 3),

                  Obx(
                    () => Visibility(
                      visible: controller.loadMore.value || controller.loadMoreFromServer.value,
                      child: const Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ),

                  Obx(
                    () => Visibility(
                      visible: controller.errorMsg.value.isNotEmpty,
                      child: Center(
                        child: Text(
                          controller.errorMsg.value,
                          textAlign: TextAlign.center,
                          style: const TextStyle(
                            color: Colors.red,
                          ),
                        ),
                      ),
                    ),
                  ),

                  const SizedBox(height: 10),
                ],
              );
            }

            return _repoItem(context, controller.repositories[index], index);
          },
        ),
      );

  Widget _repoItem(BuildContext context, Repository repository, int index) =>
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Material(
          type: MaterialType.transparency,
          child: InkWell(
            borderRadius: BorderRadius.circular(5.0),
            onTap: () => controller.onRepositoryClick(index),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(12, 12, 12, 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  _top(context, repository, index),
                  const SizedBox(height: 3),
                  const Divider(
                    indent: 23,
                    color: Colors.grey,
                    thickness: .18,
                  ),
                  const SizedBox(height: 10),
                  _description(context, repository),
                  const SizedBox(height: 10),
                  _topics(context, repository),
                  const SizedBox(height: 20),
                  _bottom(context, repository),
                ],
              ),
            ),
          ),
        ),
      );

  // serial number
  // name
  // last update
  // default branch
  Widget _top(BuildContext context, Repository repository, int index) => Row(
    crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // serial
          Transform.translate(
            offset: const Offset(0, 4),
            child: Text(
              "${index+1}. ",
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),

          const SizedBox(width: 10),

          // name and default branch
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  repository.name ?? "-",
                  style: Theme.of(context).textTheme.bodyText1?.copyWith(
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),

                const SizedBox(height: 5),

                Text(
                  "Owner Type: ${(repository.owner?.type ?? "-").toUpperCase()}",
                  style: GoogleFonts.montserrat(
                      color: Colors.grey,
                      fontWeight: FontWeight.w500,
                      fontSize: 10
                  ),
                ),
              ],
            ),
          ),

          // last update
          Text(
            repository.createdAt!.timeAgo,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(
              color: Colors.grey,
              fontSize: 12,
            ),
          ),
        ],
      );

  // description
  Widget _description(BuildContext context, Repository repository) => Text(
        repository.description ?? "No description available",
        maxLines: 3,
        overflow: TextOverflow.ellipsis,
        style: const TextStyle(
          color: Colors.grey,
          fontSize: 13,
        ),
      );

  // topics
  Widget _topics(BuildContext context, Repository repository) => Wrap(
    spacing: 10,
    runSpacing: 2,
    children: [
      ...(repository.topics ?? []).asMap().entries.map((e) {
        if(e.key > 5) return const SizedBox();

        return _topicItem(context, e.value ?? "");
      }),
    ],
  );

  Widget _topicItem(BuildContext context, String name) => Visibility(
        visible: name.isNotEmpty,
        child: Text(
          "#$name ",
          style: const TextStyle(
            color: Colors.blue,
          ),
        ),
      );

  // language
  // default branch
  Widget _bottom(BuildContext context, Repository repository) => Padding(
    padding: const EdgeInsets.symmetric(horizontal: 15),
    child: Row(
          children: [
            // language
            Container(
              width: 7,
              height: 7,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.redAccent,
              ),
            ),
            const SizedBox(width: 7),
            Text(
              repository.language ?? "--",
              style: GoogleFonts.roboto(
                textStyle: Theme.of(context).textTheme.bodyText1,
              ),
            ),

            const Spacer(),

            // star
            const FaIcon(
              FontAwesomeIcons.star,
              color: Colors.amber,
              size: 12,
            ),
            const SizedBox(width: 7),
            Text(
              (repository.stargazersCount ?? 0).compactShort,
              style: GoogleFonts.nunito(
                textStyle: Theme.of(context).textTheme.bodyText1,
              ),
            ),

            const Spacer(),

            // default branch
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
              decoration: BoxDecoration(
                color: Colors.blue.withOpacity(.1),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  const FaIcon(
                    FontAwesomeIcons.codeBranch,
                    color: Colors.blue,
                    size: 12,
                  ),
                  const SizedBox(width: 7),
                  Text(
                    repository.defaultBranch ?? "--",
                    style: GoogleFonts.ptSans(
                      color: Colors.blue,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
  );
}
