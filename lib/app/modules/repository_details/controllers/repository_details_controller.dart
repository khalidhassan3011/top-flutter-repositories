import 'package:top_flutter_repositories/app/repository/api_helper.dart';

import '../../../common/utils/exports.dart';
import '../../../models/repository.dart';
import '../interface/repository_details_interface.dart';

class RepositoryDetailsController extends GetxController implements RepositoryDetailsInterface {
  late Repository repository;

  final ApiHelper _apiHelper = Get.find();

  /// [loading] will true until api call not finished
  RxBool loading = true.obs;

  @override
  void onInit() {
    repository = Get.arguments[MyStrings.data];

    _getOwnerDetails();

    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void onBackClick() {
    Get.back();
  }

  @override
  void onFollowClick() {
    CustomMessage.snackbar("Coming Soon");
  }

  Future<void> _getOwnerDetails() async {
    await _apiHelper.getOwnerDetails(repository.owner!.login!).then((response) {
      response.fold((l) {
        loading.value = false;
      }, (r) {
        repository.owner = r;
      });

      loading.value = false;
    });
  }
}
