import '../../../common/utils/exports.dart';
import '../../../common/utils/utils.dart';
import '../controllers/repository_details_controller.dart';

class RepositoryDetailsView extends GetView<RepositoryDetailsController> {
  const RepositoryDetailsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => controller.loading.value
            ? const Center(child: CircularProgressIndicator())
            : Column(
                children: [
                  _profile(context),
                  const SizedBox(height: 50),
                  _counter(context),
                  const SizedBox(height: 20),
                  _recentRepositoryView(context),
                ],
              ),
      ),
    );
  }

  // profile section
  Widget _profile(BuildContext context) => Stack(
        clipBehavior: Clip.none,
        children: [

          const SizedBox(
            height: 300,
          ),

          // background
          Image.asset(
            MyAssets.profileImageBg,
            height: 280,
            width: double.infinity,
            fit: BoxFit.cover,
          ),

          // owner info
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            top: 0,
            child: Column(
              children: [
                const SizedBox(height: 50),
                _image,
                const SizedBox(height: 15),
                _name,
                const SizedBox(height: 2),
                _location,
                const SizedBox(height: 10),
                _bio,
              ],
            ),
          ),

          // follow button
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: _followBtn,
          ),

          // back button
          Positioned(
            left: 10,
            top: 30,
            child: InkWell(
              borderRadius: BorderRadius.circular(20),
              onTap: controller.onBackClick,
              child: Container(
                width: 40,
                height: 40,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: const Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        ],
      );

  Widget get _image => Align(
        child: Container(
          height: 90,
          width: 90,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.grey,
            border: Border.all(
              color: Colors.white,
              width: 2,
            ),
          ),
          child: Container(
            margin: const EdgeInsets.all(1),
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Image.network(
                controller.repository.owner!.avatarUrl!,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
      );

  Widget get _name => Text(
        controller.repository.owner?.name ?? "--",
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 18,
        ),
      );

  Widget get _location => Visibility(
        visible: (controller.repository.owner?.location ?? "").isNotEmpty,
        child: Text(
          controller.repository.owner?.location ?? "",
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: 13,
            color: Colors.grey,
          ),
        ),
      );

  Widget get _bio => Visibility(
        visible: (controller.repository.owner?.bio ?? "").isNotEmpty,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Text(
            controller.repository.owner?.bio ?? "",
            maxLines: 3,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              fontSize: 13,
              color: Colors.grey,
            ),
          ),
        ),
      );

  Widget get _followBtn => Align(
        child: InkWell(
          borderRadius: BorderRadius.circular(100),
          onTap: controller.onFollowClick,
          child: Container(
            width: 120,
            height: 40,
            padding: const EdgeInsets.symmetric(horizontal: 7),
            decoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.circular(100),
            ),
            child: const Center(
              child: Text(
                MyStrings.follow,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      );

  Widget _counter(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _counterItem(context, (controller.repository.owner?.publicRepos ?? 0).compactShort, "repositories"),
          _counterItem(context, (controller.repository.stargazersCount ?? 0).compactShort, "stars"),
          _counterItem(context, (controller.repository.owner?.followers ?? 0).compactShort, "followers"),
          _counterItem(context, (controller.repository.owner?.following ?? 0).compactShort, "following"),
        ],
      );

  Widget _counterItem(BuildContext context, String value, String title) => Column(
        children: [
          Text(
            value,
            style: Theme.of(context).textTheme.bodyText1?.copyWith(
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),

          const SizedBox(height: 3),

          Text(
            title,
            style: const TextStyle(
              color: Colors.grey,
              fontSize: 12,
            ),
          ),
        ],
      );

  Widget _recentRepositoryView(BuildContext context) => Container(
        margin: const EdgeInsets.symmetric(horizontal: 14, vertical: 16),
        child: Card(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 7),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Text(
                  "Recently Visited Repository",
                  style: Theme.of(context).textTheme.bodyText1?.copyWith(
                    fontSize: 12,
                    color: Colors.grey,
                  ),
                ),

                const Divider(),

                Text(
                  controller.repository.name ?? "--",
                  style: Theme.of(context).textTheme.bodyText1?.copyWith(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                ),

                const SizedBox(height: 10),

                Text(
                  controller.repository.description ?? "--",
                  maxLines: 5,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodyText1,
                ),

                const SizedBox(height: 12),

                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                      "Last Update: ${Utils.dateFormat(controller.repository.updatedAt!)}",
                      style: const TextStyle(
                        color: Colors.grey,
                        fontSize: 12,
                      )
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
