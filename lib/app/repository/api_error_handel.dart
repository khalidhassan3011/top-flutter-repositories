import 'package:dartz/dartz.dart';
import 'package:get/get.dart';

import '../common/values/my_strings.dart';

class ApiErrorHandle {
  static Either<String, bool> checkError(Response response) {
    switch (response.statusCode) {
      case 200:
        return right(true);
      case 422:
        return left(response.body["message"]);
      default:
        return left(MyStrings.somethingWrong);
    }
  }
}
