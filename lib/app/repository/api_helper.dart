import '../common/utils/type_def.dart';
import '../enums/repository_sort_option.dart';
import '../models/owner.dart';
import '../modules/home/model/search_repositories.dart';

abstract class ApiHelper {
  EitherModel<SearchRepositories> getSearchRepositories({
    int? page,
    int? perPageItem,
    RepositorySortOption? sortBy,
  });

  EitherModel<Owner> getOwnerDetails(String userName);

  EitherModel<Owner> getFollowers(String userName);

  EitherModel<Owner> getFollowing(String userName);
}
