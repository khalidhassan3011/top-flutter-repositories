import 'package:dartz/dartz.dart';

import '../common/utils/exports.dart';
import '../common/utils/logcat.dart';
import '../common/utils/type_def.dart';
import '../enums/repository_sort_option.dart';
import '../models/owner.dart';
import '../modules/home/model/search_repositories.dart';
import 'api_error_handel.dart';
import 'api_helper.dart';

part 'api_urls.dart';

class ApiHelperImpl extends GetConnect with ApiHelper {
  @override
  void onInit() {
    httpClient.baseUrl = _ApiUrls.base;
    httpClient.timeout = const Duration(seconds: 120);

    httpClient.addRequestModifier<dynamic>((request) {
      Logcat.msg(request.url.toString());
      return request;
    });
  }

  /// basically we need to convert [Response] to model
  /// that's why [Response] must contains a body of Map type
  /// if [Response] is null that means their is NO INTERNET CONNECTION
  ///
  /// [Response] will be null or
  /// [Response] data or type will not match with model field
  /// so we must verify [Response] data is correct format
  ///
  /// way to check correct data t
  ///  1. check null for no internet
  ///  2. check status code is valid or not
  ///  3. finally convert response to model
  Either<String, T> _convert<T>(
    Response? response,
    Function(Map<String, dynamic>) base,
  ) {
    try {
      if (response == null || response.statusCode == null || response.statusText!.contains("SocketException")) return left(MyStrings.noInternetConnection);

      Either<String, bool> hasError = ApiErrorHandle.checkError(response);

      if (hasError.isLeft()) return left(hasError.asLeft);

      return right(base(response.body) as T);
    } catch (e, s) {
      Logcat.msg(e.toString());
      Logcat.stack(s);

      return left(e.toString());
    }
  }

  @override
  EitherModel<SearchRepositories> getSearchRepositories({
    int? page,
    int? perPageItem,
    RepositorySortOption? sortBy,
  }) async {
    String url = "search/repositories?q=Flutter";

    if (page != null) url += "&page=$page";
    if (perPageItem != null) url += "&per_page=$perPageItem";
    if (sortBy != null) url += "&sort=${sortBy.urlKeyword}";

    Response response = await get(url);

    return _convert<SearchRepositories>(
      response,
      SearchRepositories.fromJson,
    ).fold((l) => left(l), (r) => right(r));
  }

  @override
  EitherModel<Owner> getOwnerDetails(String userName) async {
    var response = await get("users/$userName");

    return _convert<Owner>(
      response,
      Owner.fromJson,
    ).fold((l) => left(l), (r) => right(r));
  }

  @override
  EitherModel<Owner> getFollowers(String userName)  async {
    var response = await get("users/$userName/followers");

    return _convert<Owner>(
      response,
      Owner.fromJson,
    ).fold((l) => left(l), (r) => right(r));
  }

  @override
  EitherModel<Owner> getFollowing(String userName) async {
    var response = await get("users/$userName/following");

    return _convert<Owner>(
      response,
      Owner.fromJson,
    ).fold((l) => left(l), (r) => right(r));
  }
}
