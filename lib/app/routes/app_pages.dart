import 'package:get/get.dart';

import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/repository_details/bindings/repository_details_binding.dart';
import '../modules/repository_details/views/repository_details_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const home = Routes.home;

  static final routes = [
    GetPage(
      name: _Paths.home,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.repositoryDetails,
      page: () => const RepositoryDetailsView(),
      binding: RepositoryDetailsBinding(),
    ),
  ];
}
