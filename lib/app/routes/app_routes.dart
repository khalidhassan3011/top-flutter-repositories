part of 'app_pages.dart';

abstract class Routes {
  Routes._();

  static const home = _Paths.home;
  static const repositoryDetails = _Paths.repositoryDetails;
}

abstract class _Paths {
  _Paths._();

  static const home = '/home';
  static const repositoryDetails = '/repository-details';
}
