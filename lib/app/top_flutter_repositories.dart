import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'common/app_info/app_info.dart';
import 'common/style/theme.dart';
import 'common/utils/initializer.dart';
import 'routes/app_pages.dart';

class TopFlutterRepositories extends StatelessWidget {
  const TopFlutterRepositories({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: AppInfo.appName,
      initialRoute: Routes.home,
      getPages: AppPages.routes,
      initialBinding: InitialBindings(),
      debugShowCheckedModeBanner: false,
      defaultTransition: Transition.rightToLeft,
      theme: AppTheme.light,
      darkTheme: AppTheme.dark,
      themeMode: ThemeMode.system,
    );
  }
}
