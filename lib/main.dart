import 'package:flutter/material.dart';

import 'app/common/utils/initializer.dart';
import 'app/top_flutter_repositories.dart';

void main() {
  Initializer.instance.init(() {
    runApp(const TopFlutterRepositories());
  });
}
