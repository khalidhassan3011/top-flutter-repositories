import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:top_flutter_repositories/app/common/utils/exports.dart';

void main() {
  test("Extension test on Either<L, R>", () {
    expect(_eitherTextLeft().isLeft(), true);

    // test extension
    expect(_eitherTextLeft().asLeft, "left");

    expect(_eitherTextLeft().isRight(), false);

    // test extension
    expect(_eitherTextRight().asRight, "right");
  });

  group("Time Difference", () {

    test("difference in minute", () {
      expect(DateTime.now().subtract(const Duration(minutes: 15)).toString().timeDifferenceInMinute, 15);
      expect(DateTime.now().subtract(const Duration(minutes: 30)).toString().timeDifferenceInMinute, isNot(15));
    });

    test("time ago", () {
      expect(DateTime.now().subtract(const Duration(minutes: 15)).toString().timeAgo, "15m");
      expect(DateTime.now().subtract(const Duration(minutes: 59)).toString().timeAgo, "59m");
      expect(DateTime.now().subtract(const Duration(minutes: 60)).toString().timeAgo, "1h");
      expect(DateTime.now().subtract(const Duration(hours: 23)).toString().timeAgo, "23h");
      expect(DateTime.now().subtract(const Duration(hours: 24)).toString().timeAgo, "1d");
      expect(DateTime.now().subtract(const Duration(hours: 30)).toString().timeAgo, "1d");
      expect(DateTime.now().subtract(const Duration(hours: 60)).toString().timeAgo, "2d");
      expect(DateTime.now().subtract(const Duration(days: 364)).toString().timeAgo, "364d");
      expect(DateTime.now().subtract(const Duration(days: 365)).toString().timeAgo, "1y");
    });

  });
}

Either<String, String> _eitherTextLeft() {
  return left("left");
}

Either<String, String> _eitherTextRight() {
  return right("right");
}